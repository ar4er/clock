import { createElement } from "./modules/functions.js";

const app = document.getElementById("app");

const clockFace = createElement("div", "clock-face");
const secondHand = createElement("div", "second-hand");
const secondHandPointer = createElement("div", "second-hand__pointer");
const minuteHand = createElement("div", "minute-hand");
const minuteHandPointer = createElement("div", "minute-hand__pointer");
const hourHand = createElement("div", "hour-hand");
const hourHandPointer = createElement("div", "hour-hand__pointer");

secondHand.append(secondHandPointer);
minuteHand.append(minuteHandPointer);
hourHand.append(hourHandPointer);
app.append(clockFace);
clockFace.append(secondHand, minuteHand, hourHand);

const mainTime = [
  { tagName: "div", className: "number-12" },
  { tagName: "div", className: "number-3" },
  { tagName: "div", className: "number-6" },
  { tagName: "div", className: "number-9" },
];
const secondaryTime = [
  { tagName: "div", className: "number-1" },
  { tagName: "div", className: "number-2" },
  { tagName: "div", className: "number-4" },
  { tagName: "div", className: "number-5" },
  { tagName: "div", className: "number-7" },
  { tagName: "div", className: "number-8" },
  { tagName: "div", className: "number-10" },
  { tagName: "div", className: "number-11" },
];

mainTime.forEach((number) => {
  const clockNumber = createElement(number.tagName, number.className);
  clockNumber.style.width = "50px";
  clockNumber.style.border = "3px solid black";
  clockNumber.style.position = "absolute";
  clockFace.append(clockNumber);
});
secondaryTime.forEach((number) => {
  const clockNumber = createElement(number.tagName, number.className);
  clockFace.append(clockNumber);
  clockNumber.style.width = "35px";
  clockNumber.style.border = "1px solid black";
  clockNumber.style.position = "absolute";
});

window.onload = function () {
  let date = false;

  let clock;

  clock = setInterval(() => {
    date = new Date();
    const rotateDegreeSeconds = date.getSeconds() * 6 - 90;
    const rotateDegreeMinutes = date.getMinutes() * 6 - 90;
    let rotateDegreeHours;
    const hourError = date.getMinutes() / 2;
    if (date.getHours() <= 12) {
      rotateDegreeHours = date.getHours() * 30 - 90 + hourError;
    } else if (date.getHours() > 12) {
      rotateDegreeHours = (date.getHours() - 12) * 30 - 90 + hourError;
    }
    secondHand.style.rotate = `${rotateDegreeSeconds}deg`;
    minuteHand.style.rotate = `${rotateDegreeMinutes}deg`;
    hourHand.style.rotate = `${rotateDegreeHours}deg`;
  }, 1000);

  document.addEventListener("click", () => {
    if (date) {
      clearInterval(clock);
      date = false;
    } else if (!date) {
      clock = setInterval(() => {
        date = new Date();
        const rotateDegreeSeconds = date.getSeconds() * 6 - 90;
        const rotateDegreeMinutes = date.getMinutes() * 6 - 90;
        let rotateDegreeHours;
        const hourError = date.getMinutes() / 2;
        if (date.getHours() <= 12) {
          rotateDegreeHours = date.getHours() * 30 - 90 + hourError;
        } else if (date.getHours() > 12) {
          rotateDegreeHours = (date.getHours() - 12) * 30 - 90 + hourError;
        }
        secondHand.style.rotate = `${rotateDegreeSeconds}deg`;
        minuteHand.style.rotate = `${rotateDegreeMinutes}deg`;
        hourHand.style.rotate = `${rotateDegreeHours}deg`;
      }, 1000);
    }
  });
};
